package com.michalrys.calculator;

import com.michalrys.calculator.commands.Command;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Calculator {
    private LinkedList<String> parameters;
    private HashMap<Character, Command> commands = new HashMap<>();
    private Scanner scanner = new Scanner(System.in);

    public void setCalculation(char shortcut, Command command) {
        commands.put(shortcut, command);
    }

    public String execute(char shortcut) {
        if (!commands.keySet().contains(shortcut)) {
            return "ERROR - no such operation";
        }
        Command command = commands.get(shortcut);
        return command.execute(parameters);
    }

    public void setParameters(int howManyValues) {
        parameters = new LinkedList<>();
        for (int i = 1; i <= howManyValues; i++) {
            System.out.printf("Value %d:\n", i);
            parameters.add(scanner.nextLine());
        }
    }

    public List<String> getParameters() {
        return parameters;
    }
}