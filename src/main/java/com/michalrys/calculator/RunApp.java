package com.michalrys.calculator;

import com.michalrys.calculator.commands.*;

public class RunApp {
    public static void main(String[] args) {
        // Design Pattern = Command
        Calculator calc = new Calculator();

        calc.setCalculation('+', new CommandAddAllValues());
        calc.setCalculation('-', new CommandSubstractValuesFromFirstGiven());
        calc.setCalculation('*', new CommandMultiplyAllValues());
        calc.setCalculation('%', new CommandFirstValueModuloSecond());
        calc.setCalculation('w', new CommandHowManyPolishWomansNames());

        calc.setParameters(3);

        System.out.printf("%s -> add all = %s\n", calc.getParameters(), calc.execute('+'));
        System.out.printf("%s -> multiply all = %s\n", calc.getParameters(), calc.execute('*'));
        System.out.printf("%s -> substract all from first = %s\n", calc.getParameters(), calc.execute('-'));
        System.out.printf("%s -> first value modulo of second value = %s\n", calc.getParameters(), calc.execute('%'));

        calc.setParameters(3);
        System.out.printf("%s -> how many women? = %s\n", calc.getParameters(), calc.execute('w'));

    }
}
