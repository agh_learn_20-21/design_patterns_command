package com.michalrys.calculator.commands;

import java.util.List;

public class CommandFirstValueModuloSecond implements Command {
    @Override
    public String execute(List<String> parameters) {
        if (notNumericalValues(parameters)) {
            return "ERROR -> numerical values are needed.";
        }
        if (parameters.size() < 2 || Double.parseDouble(parameters.get(0)) < Double.parseDouble(parameters.get(1))) {
            return "ERROR -> two values are needed, first one must be greater than second one.";
        }
        double results = Double.parseDouble(parameters.get(0)) % Double.parseDouble(parameters.get(1));
        return String.format("%.2f", results);
    }

    private boolean notNumericalValues(List<String> parameters) {
        for (String parameter : parameters) {
            try {
                Double.parseDouble(parameter);
            } catch (NumberFormatException exc) {
                return true;
            }
        }
        return false;
    }
}