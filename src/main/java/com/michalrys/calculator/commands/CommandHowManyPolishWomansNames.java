package com.michalrys.calculator.commands;

import java.util.List;

public class CommandHowManyPolishWomansNames implements Command {
    @Override
    public String execute(List<String> parameters) {
        int amount = 0;
        for (String name : parameters) {
            char lastLetter = name.toLowerCase().charAt(name.length() - 1);
            if (lastLetter == 'a') {
                amount++;
            }
        }
        return String.valueOf(amount);
    }
}
