package com.michalrys.calculator.commands;

import java.util.List;

public class CommandSubstractValuesFromFirstGiven implements Command {
    @Override
    public String execute(List<String> parameters) {
        if(notNumericalValues(parameters)) {
            return "ERROR -> cannot substract not numerical values.";
        }
        if(parameters.size() <= 1) {
            return "ERROR -> at least 2 values are needed.";
        }
        double results = Double.parseDouble(parameters.get(0));
        for(int i = 1; i < parameters.size(); i++) {
            results -= Double.parseDouble(parameters.get(i));
        }
        return String.format("%.2f", results);
    }

    private boolean notNumericalValues(List<String> parameters) {
        for(String parameter: parameters) {
            try {
                Double.parseDouble(parameter);
            } catch (NumberFormatException exc) {
                return true;
            }
        }
        return false;
    }
}