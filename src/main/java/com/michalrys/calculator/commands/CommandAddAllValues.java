package com.michalrys.calculator.commands;

import java.util.List;

public class CommandAddAllValues implements Command{

    @Override
    public String execute(List<String> parameters) {
        if(notNumericalValues(parameters)) {
            return "ERROR -> cannot add not numerical values.";
        }
        double results = 0;
        for(String value : parameters) {
            results += Double.valueOf(value);
        }
        return String.format("%.2f", results);
    }

    private boolean notNumericalValues(List<String> parameters) {
        for(String parameter: parameters) {
            try {
                Double.parseDouble(parameter);
            } catch (NumberFormatException exc) {
                return true;
            }
        }
        return false;
    }
}