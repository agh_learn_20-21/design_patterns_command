package com.michalrys.calculator.commands;

import java.util.List;

public class CommandMultiplyAllValues implements Command {
    @Override
    public String execute(List<String> parameters) {
        if (notNumericalValues(parameters)) {
            return "ERROR -> cannot multiply not numerical values.";
        }
        if (parameters.size() == 0) {
            return "ERROR -> no values to multiply";
        }
        double results = 1;
        for (String value : parameters) {
            results *= Double.valueOf(value);
        }
        return String.format("%.2f", results);
    }

    private boolean notNumericalValues(List<String> parameters) {
        for (String parameter : parameters) {
            try {
                Double.parseDouble(parameter);
            } catch (NumberFormatException exc) {
                return true;
            }
        }
        return false;
    }
}
